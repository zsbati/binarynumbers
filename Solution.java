import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import java.util.Arrays;

public class Solution {



    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        
        scanner.close();
        
        String bin = "";
    int m = n;
    while (m != 0){
      bin = m%2 + bin;
      m = (m - m%2)/2;
    }
    //System.out.println(bin);
    String[] bin_arr = bin.split("0");
    
    int max = 0;
    for (int i=0; i< bin_arr.length; i++){
    
      if (bin_arr[i].length() > max){
        max = bin_arr[i].length();
      }
    }
    System.out.println(max);
    }
}

